const convict = require('convict');

const config = convict({
  env: {
    doc: 'The application environment.',
    format: ['prod', 'dev', 'test'],
    default: 'dev',
    env: 'NODE_ENV'
  },
  ip: {
    doc: 'The IP address to bind.',
    format: 'ipaddress',
    default: '127.0.0.1',
    env: 'IP_ADDRESS'
  },
  port: {
    doc: 'The port to bind.',
    format: 'port',
    default: 8080,
    env: 'PORT',
    arg: 'port'
  },
  db: {
    uri: {
      doc: 'The MongoDB URI.',
      default: 'mongodb://localhost/vimps',
      env: 'MONGODB_URI'
    }
  },
  storage: {
    uri: {
      doc: 'The Cloudinary URI.',
      default: 'eggs',
      env: 'CLOUDINARY_URI'
    },
    cloud_name: {
      doc: 'The Cloudinary cloud name.',
      default: 'cloudname'
    },
    api_key: {
      doc: 'The Cloudinary API key.',
      default: 'apikey'
    },
    api_secret: {
      doc: 'The Cloudinary API secret.',
      default: 'apisecret'
    }
  },
  auth: {
    secret: {
      doc: 'The JWT secret.',
      default: 'keyboard cat',
      env: 'JWT_SECRET'
    }
  }
});

const env = config.get('env');
config.loadFile('./config.' + env + '.json');

config.validate({ allowed: 'strict' });

module.exports = config;
