const express = require('express');
const router = express.Router();
const cloudinary = require('cloudinary');
// const fileUpload = require('express-fileupload');
var multer = require('multer');
var storage = multer.memoryStorage();
var upload = multer({ storage: storage });

const Issue = require('../models/issue');
const House = require('../models/house');

// router.use(fileUpload());

router.get('/:houseId', (req, res, next) => {
  var house;  

  House.findById(req.params.houseId)
    .populate('street')
    .then((res) => {
      house = res;

      return Issue.find({ house: house._id })
        .populate('worker')
        .populate({
          path: 'house',
          populate: {
            path: 'street'
          }
        })
        .exec();
    })
    .then((issues) => {
      res.json({
        success: true,
        house: house,
        issues: issues
      });
    });
});

router.post('/:houseId/passport', upload.any(), (req, res, next) => {
  var house;

  House.findById(req.params.houseId)
    .then((res) => {
      house = res;

      if (!house) {
        throw new Error('Did not find a house with the id ' + req.params.houseId);
      }

      house.passport = req.body;

      return true;
    })
    .then(() => {
      if (req.files) {
        var uploaders = [];

        for (let file of req.files) {
          uploaders.push(new Promise((resolve, reject) => {
            cloudinary.v2.uploader.upload_stream(function (err, data) {
              if (err) {
                reject(err);
              } else {
                house.img.push(data.secure_url);
                resolve();
              }
            })
            .end(file.buffer);
          }));
        }

        return Promise.all(uploaders);
      } else {
        return true;
      }
    })
    .then(() => {
      return house.save();
    })
    .then(() => {
      console.log(house);
      res.json({
        success: true,
        house: house
      });
    })
    .catch((error) => {
      next(error);
    });
});

module.exports = router;
