const express = require('express');
const config = require('../config');
const jwt = require('express-jwt');
const router = express.Router();

const usersRouter = require('./users');
const streetsRouter = require('./streets');
const issuesRouter = require('./issues');
const housesRouter = require('./houses');
const authRouter = require('./auth');

router.use('/auth', authRouter);

router.use(jwt({
  secret: config.get('auth.secret'),
  getToken: function fromHeaderOrQueryString(req) {
    if (req.query && req.query.token) {
      return req.query.token;
    }

    return null;
  }
}));
router.use('/users', usersRouter);
router.use('/issues', issuesRouter);
router.use('/streets', streetsRouter);
router.use('/houses', housesRouter);

module.exports = router;
