const express = require('express');
const jwt = require('jsonwebtoken');
const jwtVerify = require('express-jwt');
const config = require('../config');
const router = express.Router();
const mongoose = require('mongoose');

const User = mongoose.model('User');

router.post('/signup', (req, res, next) => {
  User.create({
      login: req.body.login,
      password: req.body.password,
      name: req.body.name,
      permissions: ['worker']
    })
    .then((user) => {
      const payload = {
        _id: user._id,
        name: user.name,
        permissions: user.permissions
      };

      const token = jwt.sign(payload, config.get('auth.secret'));

      res.json({
        success: true,
        token: token
      });
    });
});

router.post('/login', (req, res, next) => {
  User.findOne({ login: req.body.login, password: req.body.password })
    .then((user) => {
      if (user) {
        const payload = {
          _id: user._id,
          name: user.name,
          permissions: user.permissions,
          img: user.img
        };

        const token = jwt.sign(payload, config.get('auth.secret'));

        res.json({
          success: true,
          token: token,
          user: payload
        });
      } else {
        throw new Error('Неправильный логин и/или пароль');
      }
    })
    .catch((err) => {
      next(err);
    });
});

module.exports = router;
