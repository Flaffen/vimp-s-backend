const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const cloudinary = require('cloudinary');
const fileUpload = require('express-fileupload');
const _ = require('lodash');

const Issue = require('../models/issue');
const Street = require('../models/street');
const House = require('../models/house');

router.use(fileUpload());

router.get('/', (req, res, next) => {
  var q;

  if (req.user.permissions.indexOf('worker') !== -1) {
    q = Issue.find({ worker: req.user._id });
  } else if (req.user.permissions.indexOf('admin') === -1) {
    q = Issue.find({ created_by: req.user._id });
  } else {
    q = Issue.find();
  }

  q.populate({
      path: 'house',
      populate: {
        path: 'street'
      }
    })
    .populate('worker')
    .populate('created_by')
    .exec()
    .then((issues) => {
      res.json({
        success: true,
        issues: issues
      });
    })
    .catch((err) => {
      next(err);
    });
});

router.post('/', (req, res, next) => {
  Street.findOne({ name: req.body.street })
    .then((street) => {
      return House.findOne({ street: street._id, num: req.body.house });
    })
    .then((house) => {
      return Issue.create({
        type_of_work: req.body.type_of_work,
        tel: req.body.tel,
        description: req.body.description,
        status: 1,
        house: house._id,
        created_by: req.user._id,
        created_at: new Date(),
      });
    })
    .then((issue) => {
      if (issue) {
        res.json({
          success: true,
          issue: issue
        });
      }
    })
    .catch((err) => {
      next(err);
    });
});

router.get('/:issueId', (req, res, next) => {
  Issue.findById(req.params.issueId)
    .populate({
      path: 'house',
      populate: {
        path: 'street'
      }
    })
    .populate('worker')
    .exec()
    .then((issue) => {
      if (issue) {
        res.json({
          success: true,
          issue: issue
        });
      } else {
        throw new Error('No such issue');
      }
    })
    .catch((err) => {
      next(err);
    });
});

router.post('/:issueId', (req, res, next) => {
  var issue;

  Issue.findById(req.params.issueId)
    .then((results) => {
      issue = results;

      if (req.user.permissions.indexOf('admin') !== -1) {
        Street.findOne({ name: req.body.house.street.name })
          .then((street) => {
            House.findOne({ street: street._id })
              .then((house) => {
                issue.house = house._id;
              });
          });
      }

      return true;
    })
    .then(() => {
      if (req.user.permissions.indexOf('admin') !== -1) {
        issue.set({
          type_of_work: req.body.type_of_work,
          tel: req.body.tel,
          description: req.body.description,
          status: req.body.status,
          worker: req.body.worker._id,
        });

        return true;
      } else {
        issue.set({
          // metering: req.body.metering,
          comment: req.body.comment,
          // apartments: req.body.apartments.split(' '),
          status: 3,
          closed_at: new Date()
        });

        if (!_.isEmpty(req.files)) {
          console.log(req.files);
          const photos = req.files['photo-load'];
          console.log(photos);

          if (Array.isArray(photos)) {
            var uploaders = [];

            for (let photo of photos) {
              var p = new Promise((resolve, reject) => {
                cloudinary.v2.uploader.upload_stream((err, res) => {
                  if (err) {
                    reject(err);
                  }

                  resolve(res);
                }).end(photo.data);
              });

              uploaders.push(p);
            }

            return Promise.all(uploaders);
          } else {
            return new Promise((resolve, reject) => {
              cloudinary.v2.uploader.upload_stream((err, res) => {
                if (err) {
                  reject(err);
                }

                resolve(res);
              }).end(photos.data);
            });
          }
        } else {
          return true;
        }
      }
    })
    .then((results) => {
      if (!_.isEmpty(req.files)) {
        issue.img = [];

        if (Array.isArray(results)) {
          results.map(entry => issue.img.push(entry.secure_url));
        } else {
          issue.img.push(results.secure_url);
        }
      }

      return issue.save();
    })
    .then((updatedIssue) => {
      res.json({
        success: true,
        issue: updatedIssue
      });
    })
    .catch((err) => {
      next(err);
    });
});

router.get('/:issueId/delete', (req, res, next) => {
  Issue.findById(req.params.issueId)
    .then((issue) => {
      if (issue.img.length > 0) {
        var imagesToDelete = issue.img.map((img) => {
          return img.substring(img.lastIndexOf('/') + 1, img.lastIndexOf('.'));
        });

        cloudinary.v2.api.delete_resources(imagesToDelete, function (err, results) {
          if (err) {
            next(err);
          } else {
            issue.remove(function () {
              res.json({
                success: true
              });
            });
          }
        });
      } else {
        issue.remove(function () {
          res.json({
            success: true
          });
        });
      }
    });
});

module.exports = router;
