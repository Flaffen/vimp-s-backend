const express = require('express');
const jwt = require('express-jwt');
const config = require('../config');
const router = express.Router();

const User = require('../models/user');

router.get('/', (req, res, next) => {
  const role = req.query.role;

  User.find({ 'permissions': role })
    .then((users) => {
      if (users.length === 0) {
        throw new Error('No such users found');
      }

      res.json({
        success: true,
        users: users
      });
    })
    .catch((err) => {
      next(err);
    });
});

router.get('/token', (req, res, next) => {
  res.json({
    success: true,
    user: req.user
  });
});

module.exports = router;
