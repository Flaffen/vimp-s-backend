const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Street = require('../models/street');
const House = require('../models/house');

router.get('/', (req, res, next) => {
  Street.find()
    .then((streets) => {
      res.json({
        success: true,
        streets: streets
      });
    })
    .catch((err) => {
      next(err);
    });
});

router.get('/:streetSlug', (req, res, next) => {
  var street;

  Street.findOne({ slug: req.params.streetSlug })
    .then((res) => {
      street = res;

      return House.find({ street: street._id });
    })
    .then((houses) => {
      res.json({
        success: true,
        street: street,
        houses: houses
      });
    });
});

module.exports = router;
