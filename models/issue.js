const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const House = require('./house');
const User = require('./user');

var IssueSchema = mongoose.Schema({
  type_of_work: String,
  metering: Number,
  tel: String,
  description: String,
  img: Array,
  status: Number,
  comment: String,
  apartments: Array,
  worker: { type: Schema.Types.ObjectId, ref: 'User' },
  house: { type: Schema.Types.ObjectId, ref: 'House' },
  created_by: { type: Schema.Types.ObjectId, ref: 'User' },
  created_at: Date,
  closed_at: Date
});

module.exports = mongoose.model('Issue', IssueSchema);
