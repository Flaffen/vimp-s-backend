const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var HouseSchema = Schema({
  street: { type: Schema.Types.ObjectId, ref: 'Street' },
  num: String,
  img: [String],
  passport: {
    operating_since: String,
    floors: String,
    draining: String,
    soft_roof_area: String,
    visor_area: String,
    junctions_area: String,
    roof_material: String,
    last_main_repair_date: String,
    executor: String,
    warranty: String,
    repair_materials: String,
    last_repair_date: String,
    current_repair_executor: String,
    warranty2: String,
    current_repair_materials: String,
    additional_info: String
  }
});

module.exports = mongoose.model('House', HouseSchema);
