const mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
  login: String,
  password: String,
  name: String,
  // is_admin: Boolean,
  // is_worker: Boolean,
  // is_main_company: Boolean,
  // is_sibsidiary: Boolean,
  permissions: [String],
  img: String
});

module.exports = mongoose.model('User', UserSchema);
