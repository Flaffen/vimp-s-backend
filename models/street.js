const mongoose = require('mongoose');
const slug = require('slug');
const Schema = mongoose.Schema;

const StreetSchema = Schema({
  name: String,
  image: String,
  slug: String,
});

StreetSchema.pre('save', function (next) {
  this.slug = slug(this.name, { lower: true });
  next();
});

module.exports = mongoose.model('Street', StreetSchema);
