const path = require('path');
const express = require('express');
const logger = require('morgan');
const config = require('./config');
const cloudinary = require('cloudinary');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const createError = require('http-errors');
const helmet = require('helmet');
const cors = require('cors');

cloudinary.config({
  cloud_name: config.get('storage.cloud_name'),
  api_key: config.get('storage.api_key'),
  api_secret: config.get('storage.api_secret')
});
mongoose.connect(config.get('db.uri')).then(() => {
  console.log(`Successfully connected to the database ${config.get('db.uri')}`);
}, (err) => {
  console.log('Could not connect to the database');
  console.log(err);
});

const apiRouter = require('./routes/api');

const app = express();

app.use(helmet());
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

/* NOTE: In production use /api route
 * app.use('/api', apiRouter);
 */
app.use('/api', apiRouter);

// Catch 404 and forward to error handler.
app.use(function(req, res, next) {
  next(createError(404));
});

// Error handler.
app.use(function(err, req, res, next) {
  // Set locals, only providing error in development.
  const message = err.message;
  const error = config.get('env') === 'dev' ? err : {};
  console.log(error);

  res.status(err.status || 500);
  res.json({
  	success: false,
  	message: message,
  	error: error
  });
});

module.exports = app;
